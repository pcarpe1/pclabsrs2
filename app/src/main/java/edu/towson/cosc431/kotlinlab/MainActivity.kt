package edu.towson.cosc431.kotlinlab

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val receiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            //update UI
            val message: String? = intent?.getStringExtra(ColorService.COLOR_SERVICE_RESULT)
            chosen_color_text.text = message
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        choose_color_btn.setOnClickListener{chooseColor()}
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }

    fun chooseColor() {
        val intent = Intent(this, ChooserActivity::class.java)
        startActivity(intent)
    }
}
