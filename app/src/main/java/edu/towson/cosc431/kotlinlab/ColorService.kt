package edu.towson.cosc431.kotlinlab

import android.app.IntentService
import android.app.Service
import android.content.Intent
import android.os.IBinder
import edu.towson.cosc431.kotlinlabscratch.utils.Color
import edu.towson.cosc431.kotlinlabscratch.utils.ColorResult

class ColorService : IntentService("ColorService") {
    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            //val chosenColor = Color.valueOf(intent.getStringExtra())
        }
    }

    companion object {
        //intent extra key name
        val CHOSEN_COLOR = "CHOSEN_COLOR"
        val COLOR_SERVICE_RESULT = "COLOR_SERVICE_RESULT"
        val COLOR_SERVICE_RESULT_MESSAGE = "COLOR_SERVICE_RESULT_MESSAGE"
    }

}
