package edu.towson.cosc431.kotlinlab

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import edu.towson.cosc431.kotlinlabscratch.utils.Color
import kotlinx.android.synthetic.main.activity_chooser.*

class ChooserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_chooser)
        green_button.setOnClickListener{StartColorService(Color.Green)}
        red_button.setOnClickListener{StartColorService(Color.Red)}
        blue_button.setOnClickListener{StartColorService(Color.Blue)}
    }

    private fun StartColorService(color: Color) {
        val intent = Intent(this, ColorService::class.java)
        intent.putExtra(ColorService.CHOSEN_COLOR, color.name)
        startService(intent)
        finish()
    }
}
