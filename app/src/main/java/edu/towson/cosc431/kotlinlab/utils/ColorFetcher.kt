package edu.towson.cosc431.kotlinlabscratch.utils

/**
 * Created by randy on 4/29/18.
 */

/**
 * A color
 */
enum class Color { Red, Green, Blue }

/**
 * A Color Result - The result of the the fetchColor function
 * Contains a message (String) and a color (Color)
 */
data class ColorResult(val message: String, val color: Color)

/**
 * Does some fake processing (thread sleeping) and returns a ColorResult
 */
fun fetchColor(color: Color): ColorResult {

    Thread.sleep(5000)

    when (color) {
        Color.Red -> return ColorResult("Red is not good. Here's green.", Color.Green)
        Color.Green -> return ColorResult("Green is ok.", Color.Green)
        else -> return ColorResult("Blue is really ugly. Here's red.", Color.Red)
    }
}