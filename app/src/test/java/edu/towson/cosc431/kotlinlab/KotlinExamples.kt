package edu.towson.cosc431.kotlinlabscratch

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class KotlinExamples {
    @Test
    fun nullable_examples() {

        val x = 10;
        println(x)
        //x = 11; // not allowed

        var y = 11;
        y = 12;
        println(y)
        //y = "String" // type is infered as an Int, can't assign String

    }

    @Test
    fun list_functions() {

        val list = (1..20)

        // a list of ints are mapped to an list of Strings
        println(list.map { x -> x.toString(16) })

        // filter the list
        println(list.filter { it % 2 == 0 })

        // filter, then map
        println(list.filter { it % 2 == 0 }.map { it.toString(16) })
    }

    @Test
    fun standalone_functions() {

        fun someFunction(a: String?): String? {
            return a?.substring(0, 3)
        }

        val res = someFunction("Hello World")

        assertEquals(res, "Hel")

    }
}
